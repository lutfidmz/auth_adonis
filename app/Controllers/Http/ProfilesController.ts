import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class ProfilesController {
  public async index({ response }: HttpContextContract) {
    response.ok({
      message: "Berhasil dapat data",
      data: {
        name: "John",
        assurance_number: "1368546516843651684"
      }
    })
  }

  public async store({ response }: HttpContextContract) {
    response.ok({
      message: "Masuk Store"
    })
  }

  public async show({ response }: HttpContextContract) {
    response.ok({
      message: "Masuk Show"
    })
  }

  public async update({ }: HttpContextContract) {
    return "Masuk Update"
  }

  public async destroy({ }: HttpContextContract) {
    return "Masuk Destroy"
  }
}
