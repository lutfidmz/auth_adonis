import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
// import Hash from '@ioc:Adonis/Core/Hash'

export default class AuthController {
    public async register({ request, response }: HttpContextContract) {
        const payload = request.body()
        // payload.id = uuid()

        const data = await User.create(payload)
        response.created({
            message: "Registered",
            data
        })
    }

    public async update({ request, response, auth }: HttpContextContract) {
        const { password } = request.body()

        const data = await User.findOrFail(auth.user!.id)
        await data.merge({ password }).save()

        response.created({
            message: "Success",
            data
        })
    }
    public async login({ request, response, auth }: HttpContextContract) {
        const { email, password } = request.body()

        try {
            const token = await auth.use('api').attempt(email, password)
            return token
        } catch {
            return response.unauthorized('Invalid credentials')
        }
    }
}
